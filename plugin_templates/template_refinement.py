# TODO: Replace APPNAME with app that you are intending to integrate to xrd-tools
"""A xrd-tools refinement interface plugin that integrates APPNAME."""
import logging
import os
import shutil
import subprocess
from dataclasses import dataclass

import pandas as pd

from xrdt import refinement_interface_factory
from xrdt.refinement_interface import AppNotInstalledError, RefinementInterface

# Settings for plugin loader
# TODO: Define name of the refinement interface plugin ensure it corresponds to the one in
#       the filename of this module ("plugins/refinement_<NAME>.py")
# NAME_REFINEMENT_INTERFACE = "profex"

# Settings for plugin itself,
#   command to start the refinement app (also displayed in logs)
# TODO: Define the command to call the application that you are integrating to xrd-tools
# REFINEMENT_APPLICATION = "profex"
#   properties of input data file
INPUT_DATA_SUFFIX = ""
INPUT_DATA_HEADER = False
INPUT_DATA_DELIMITER = " "

logger = logging.getLogger(__name__)


# TODO: Change the class name to a descriptive term for the plugin, e.g. "Appname"
@dataclass
class Refiner(RefinementInterface):
    # TODO: Replace APPNAME with app that you are intending to integrate to xrd-tools
    """APPNAME refinement interface.

    Args:
        data (pd.Series): Series containing the x/y data of the measurement.
            The index represents the 2theta angle.
        project_dir (str): Path to the refinement project directory.
        data_file (str): Path to the refinement data input file.
        encoding (str): Encoding used in data file.
        suffix_data (str): Suffix appended to measurement_id to generate refinement
            input- and project- filenames.
    """

    measurement_id: str
    data: pd.Series
    dir_refinement: str
    encoding: str
    input_data_suffix: str = INPUT_DATA_SUFFIX
    input_data_header: bool = INPUT_DATA_HEADER
    input_data_delimiter: bool = INPUT_DATA_DELIMITER

    # TODO: Not required but might be helpful to check if the required app is installed
    # def _get_app_path(self) -> str:
    #     """Path to the application binary, used to check if application is installed."""
    #     return shutil.which(REFINEMENT_APPLICATION)

    # TODO: ADJUST THE CODE UNDERNEATH ACCORDING TO INPUT REQUIREMENTS
    def create_input_data(self) -> None:
        """Create refinement input data for refinement."""
        # self.data.to_csv(
        #     self.file_refinement_input,
        #     sep=self.input_data_delimiter,
        #     header=self.input_data_header,
        # )
        logger.debug(
            f"Created input data for {REFINEMENT_APPLICATION} refinement of measurement {self.measurement_id}."
        )

    # TODO: Add code to assign phases to if files and return as dict #
    def get_cif_files(self) -> dict[str, str]:
        """Get a dictionary with the name and the path to the cif files for refined phase(s)."""
        pass

    # TODO: ADD CODE TO PROVIDE THE REFINED DATA AS PANDAS DATAFRAME
    def get_refined_data(self) -> pd.DataFrame:
        """Pandas DataFrame containing refined data series."""
        pass

    # TODO: ADD CODE TO PROVIDE THE REFINEMENT RESULTS AS DICTIONARY
    def get_refinement_results(self) -> dict[str, any]:
        """Dictionary containing refinement_results."""
        pass

    # TODO: Not required, but might be helpful to get the refined data
    # @property
    # def file_refinement_input(self) -> str:
    #     """Path to refinement input data file ('.xy').

    #     It is constructed as follows:
    #     <dir_refinement>/<measurement_id><suffix_data>.xy
    #     """
    #     # return os.path.join(
    #     #     self.dir_refinement,
    #     #     self.measurement_id + self.input_data_suffix + ".xy",
    #     # )
    #     pass

    # TODO: Not required, but might be helpful to get the refined data
    # @property
    # def file_refinement_project(self) -> str:
    #     """Path to refinement project file ('.dia').

    #     It is constructed as follows:
    #     <dir_refinement>/<measurement_id><suffix_data>.dia
    #     """
    #     return os.path.join(
    #         self.dir_refinement,
    #         self.measurement_id + self.input_data_suffix + ".dia",
    #     )

    def open_refinement(self) -> None:
        """Open the refinement project.

        Raises:
            AppNotInstalledError: If profex is not installed on the machine.
        """
        # TODO: ADD A CHECK HERE TO FIGURE OUT IF APP IS INSTALLED AND RAISE OF ERROR IF NOT
        # if self._get_app_path() is None:
        #     raise AppNotInstalledError(
        #         f"Refinement application {REFINEMENT_APPLICATION!r} not installed."
        #     )
        # Get refinement project or input-data file
        if os.path.isfile(self.file_refinement_project):
            refinement_file = self.file_refinement_project
        else:
            refinement_file = self.file_refinement_input
        # Open refinement file with profex
        logger.debug(
            f"Refining measurement {self.measurement_id!r} with {REFINEMENT_APPLICATION!r}..."
        )
        # Open refinement file with application
        # TODO: ADJUST THE CODE UNDERNEATH IF NECESSARY AND FURTHER PARAMETER ARE REQUIRED
        # subprocess.run(
        #     [REFINEMENT_APPLICATION, refinement_file],
        #     stdout=subprocess.PIPE,
        #     stderr=subprocess.PIPE,
        # )


# TODO: Add the refinement interface class name to its register function if changed
def register() -> None:
    refinement_interface_factory.register(NAME_REFINEMENT_INTERFACE, Refiner)
