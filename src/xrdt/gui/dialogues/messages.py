from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QMessageBox, QPushButton


def show_missing_method_warning():
    msg_box = QMessageBox()
    msg_box.setWindowFlags(msg_box.windowFlags() | Qt.Dialog)
    msg_box.setIcon(QMessageBox.Warning)
    msg_box.setWindowTitle("Method Not Available")
    msg_box.setText("The selected method is not available.")
    msg_box.setInformativeText("This feature is currently not implemented.")
    msg_box.setStandardButtons(QMessageBox.Ok)

    button = msg_box.button(QMessageBox.Ok)
    button.clicked.connect(msg_box.close)

    msg_box.exec_()


if __name__ == "__main__":
    app = QApplication([])
    show_missing_method_warning()
    app.exec_()
