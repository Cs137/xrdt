import logging
from typing import List, Callable, Optional

from PyQt5 import QtCore, QtWidgets, QtGui

from xrdt.config import PRESETS
from xrdt.preset_manager import PresetManager
from xrdt.gui.dialogues.manager import ManagerDialog
from xrdt.gui.dialogues.messages import show_missing_method_warning

logging.basicConfig(level=logging.DEBUG)

SUBJECT_SINGULAR = "Preset"


class PresetManagerApp:
    """
    Application class for the ManagerDialog.
    """

    def __init__(self, preset_manager=PresetManager(PRESETS)):
        self.app = QtWidgets.QApplication([])
        subject_singular = SUBJECT_SINGULAR
        items = []
        if preset_manager.has_preset:
            items = preset_manager.get_names()
        self.dialog = ManagerDialog(items, subject_singular=subject_singular)

        # Connect the button signals to the corresponding functions
        self.dialog.add_button.clicked.connect(self.custom_add_action)
        self.dialog.edit_button.clicked.connect(self.custom_edit_action)
        self.dialog.remove_button.clicked.connect(self.custom_remove_action)

    def custom_add_action(self):
        """
        Custom action to perform when the Add button is clicked.
        """
        logging.debug("Custom add action executed.")
        show_missing_method_warning()
        # Implement the logic to add an item to the list

    def custom_edit_action(self):
        """
        Custom action to perform when the Edit button is clicked.
        """
        item = self.dialog.get_selected_item()
        if item is None:
            return None
        logging.debug(f"Custom edit action executed ({item!r}).")
        show_missing_method_warning()
        # Implement the logic to edit the selected item

    def custom_remove_action(self):
        """
        Custom action to perform when the Remove button is clicked.
        """
        item = self.dialog.get_selected_item()
        if item is None:
            return None
        logging.debug(f"Custom remove action executed ({item!r}).")
        show_missing_method_warning()
        # if self.dialog.list_widget.selectedItems():
        #     confirmation = QtWidgets.QMessageBox.question(
        #         self.dialog,
        #         "Confirmation",
        #         f"Do you want to remove {item}?",
        #         QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
        #         QtWidgets.QMessageBox.No,
        #     )
        #     if confirmation == QtWidgets.QMessageBox.Yes:
        #         logging.debug("Item removed: %s", item)
        #         self.dialog.items.remove(item)
        #         self.dialog._populate_list()

    def run(self):
        self.dialog.exec_()
        self.app.quit()


if __name__ == "__main__":
    app = PresetManagerApp()
    app.run()
