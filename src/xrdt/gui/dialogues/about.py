import os

from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout, QTextEdit

from xrdt import __name__ as package_name
from xrdt import __version__ as package_version

LICENSE_PATH = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "../../../../LICENSE")
)
URL_REPO = "https://codeberg.org/Cs137/xrdt"


class AboutDialog(QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowTitle(f"About {package_name}")
        self.resize(500, 500)
        layout = QVBoxLayout(self)

        # Create a QLabel widget
        label = QLabel(self)
        label.setText(f"<b>{package_name} {package_version}</b>")
        layout.addWidget(label)

        # Create a QTextEdit widget to display the license text
        license_text = QTextEdit(self)
        with open(LICENSE_PATH, "r") as file:
            license_text.setPlainText(file.read())
        license_text.setReadOnly(True)
        license_text.document().adjustSize()
        layout.addWidget(license_text)

        # Create a QLabel widget with a hyperlink
        repo_label = QLabel(self)
        repo_label.setText(f"<a href='{URL_REPO}'>Visit {package_name} on Codeberg</a>")
        repo_label.setOpenExternalLinks(True)
        layout.addWidget(repo_label)
