import logging
from typing import List, Callable, Optional

from PyQt5 import QtCore, QtWidgets, QtGui

logging.basicConfig(level=logging.DEBUG)

PREFIX_ADD = "Add"
PREFIX_EDIT = "Edit"
PREFIX_REMOVE = "Remove"
PREFIX_TITLE = "Manage"

SUBJECT_SINGULAR = "Test"


class ManagerApp(QtWidgets.QApplication):
    """
    Example application class for the ManagerDialog.
    """

    def __init__(self):
        super().__init__([])
        items = ["Device 1", "Device 2", "Device 3", "Device 4"]
        self.dialog = ManagerDialog(items, subject_singular=SUBJECT_SINGULAR)

        # Connect the button signals to the corresponding functions
        self.dialog.add_button.clicked.connect(self.custom_add_action)
        self.dialog.edit_button.clicked.connect(self.custom_edit_action)
        self.dialog.remove_button.clicked.connect(self.custom_remove_action)

    def custom_add_action(self):
        """
        Custom action to perform when the Add button is clicked.
        """
        logging.debug("Custom add action executed.")
        # Implement the logic to add an item to the list

    def custom_edit_action(self):
        """
        Custom action to perform when the Edit button is clicked.
        """
        item = self.dialog.get_selected_item()
        logging.debug(f"Custom edit action executed ({item!r}).")
        if self.dialog.list_widget.selectedItems():
            selected_item = self.dialog.list_widget.currentItem().text()
            # Implement the logic to edit the selected item

    def custom_remove_action(self):
        """
        Custom action to perform when the Remove button is clicked.
        """
        logging.debug("Custom remove action executed ({item!r}).")
        if self.dialog.list_widget.selectedItems():
            selected_item = self.dialog.list_widget.currentItem().text()
            confirmation = QtWidgets.QMessageBox.question(
                self.dialog,
                "Confirmation",
                f"Do you want to remove {selected_item}?",
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                QtWidgets.QMessageBox.No,
            )
            if confirmation == QtWidgets.QMessageBox.Yes:
                logging.debug("Item removed: %s", selected_item)
                self.dialog.items.remove(selected_item)
                self.dialog.populate_list()


class ManagerDialog(QtWidgets.QDialog):
    """
    Dialog for managing a list of items.
    """

    def __init__(
        self, items: List[str], subject_singular: str = None, subject_plural: str = None
    ):
        super().__init__()
        self.items = items
        self.subject_singular = subject_singular
        self.subject_plural = subject_plural
        self._set_subject_strings()
        self._initialise_ui()

    def _initialise_ui(self):
        """
        Initialize the user interface.
        """
        window_label = self._get_string(self.subject_plural, prefix=PREFIX_TITLE)
        self.setWindowTitle(window_label)

        self.list_widget = QtWidgets.QListWidget()
        self.list_widget.itemSelectionChanged.connect(self._update_button_states)
        self.populate_list()

        add_label = self._get_string(self.subject_singular, prefix=PREFIX_ADD)
        self.add_button = QtWidgets.QPushButton(add_label)

        edit_label = self._get_string(self.subject_singular, prefix=PREFIX_EDIT)
        self.edit_button = QtWidgets.QPushButton(edit_label)
        self.edit_button.setEnabled(False)

        remove_label = self._get_string(self.subject_singular, prefix=PREFIX_REMOVE)
        self.remove_button = QtWidgets.QPushButton(remove_label)
        self.remove_button.setEnabled(False)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.list_widget)
        layout.addWidget(self.add_button)
        layout.addWidget(self.edit_button)
        layout.addWidget(self.remove_button)

        self.setLayout(layout)

    def _set_subject_strings(self) -> None:
        """
        Set subject strings if not provided explicitly.
        """
        if self.subject_singular is not None and self.subject_plural is None:
            self.subject_plural = self.subject_singular + "s"
        if self.subject_singular is None and self.subject_plural is not None:
            if self.subject_plural.endswith("s"):
                self.subject_singular = self.subject_plural[:-1]

    def populate_list(self):
        """
        Populate the list widget with items.
        """
        self.list_widget.clear()
        for item in self.items:
            self.list_widget.addItem(item)

    def _update_button_states(self):
        """
        Update the enabled/disabled state of the buttons based on item selection.
        """
        selected_items = self.list_widget.selectedItems()
        if len(selected_items) == 1:
            self.edit_button.setEnabled(True)
            self.remove_button.setEnabled(True)
            self.edit_button.setFocus()

    @staticmethod
    def _get_string(
        body: str, prefix: str = None, suffix: str = None, separator: str = " "
    ) -> str:
        """
        Concatenate string components with optional prefix and suffix.
        """
        string = ""
        if prefix is not None:
            string += prefix + separator
        if body is not None:
            string += body
        if suffix is not None:
            string += separator + suffix
        return string.strip()

    def get_selected_item(self) -> Optional[str]:
        """
        Get the currently selected item in the list widget.
        Returns None if no item is selected.
        """
        selected_items = self.list_widget.selectedItems()
        if selected_items:
            return selected_items[0].text()
        return None


if __name__ == "__main__":
    app = ManagerApp()
    app.dialog.exec_()
