import inspect
import logging
import sys

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import (
    QDialog,
    QVBoxLayout,
    QLabel,
    QLineEdit,
    QDialogButtonBox,
    QMessageBox,
    QFormLayout,
)

from xrdt import __name__ as package_name
from xrdt.config import DEVICES, DEVICE_TYPES
from xrdt.device_manager import DeviceManager
from xrdt.gui.dialogues.manager import ManagerDialog
from xrdt.gui.dialogues.messages import show_missing_method_warning

SUBJECT_SINGULAR = "Device"


class DeviceManagerApp:
    """
    Application class for the ManagerDialog.
    """

    def __init__(self, device_manager=DeviceManager(DEVICES)):
        self.app = QtWidgets.QApplication([])

        subject_singular = SUBJECT_SINGULAR
        items = device_manager.get_type_device_list()
        self.dialog = ManagerDialog(items, subject_singular=subject_singular)
        self.device_manager = device_manager

        # Connect the button signals to the corresponding functions
        self.dialog.add_button.clicked.connect(self.custom_add_action)
        self.dialog.edit_button.clicked.connect(self.custom_edit_action)
        self.dialog.remove_button.clicked.connect(self.custom_remove_action)

    def custom_add_action(self):
        """
        Custom action to perform when the Add button is clicked.
        """
        dialog = QDialog()
        layout = QVBoxLayout(dialog)
        form_layout = QFormLayout()

        name_label = QLabel("Name:")
        name_edit = QLineEdit()
        form_layout.addRow(name_label, name_edit)

        device_type_label = QLabel("Type:")
        device_type_combo = QtWidgets.QComboBox()
        device_type_combo.addItems(self.device_manager.device_types.values())
        form_layout.addRow(device_type_label, device_type_combo)

        layout.addLayout(form_layout)

        attrs = inspect.signature(
            self.device_manager.get_creation_function(device_type_combo.currentText())
        ).parameters
        kwargs = {}

        for attr in attrs:
            line_edit = QLineEdit()
            form_layout.addRow(attr, line_edit)
            kwargs[attr] = line_edit

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        layout.addWidget(button_box)

        button_box.accepted.connect(dialog.accept)
        button_box.rejected.connect(dialog.reject)

        while dialog.exec_() == QDialog.Accepted:
            name = name_edit.text()
            if not name:
                QMessageBox.warning(
                    dialog, "Missing Name", "Please enter a name for the new device."
                )
                continue

            try:
                content = {}
                for attr, widget in kwargs.items():
                    content[attr] = widget.text()

                # Create an instance of the class using the remaining content
                device_type = device_type_combo.currentText()
                creation_function = self.device_manager.get_creation_function(
                    device_type
                )
                dev_obj = creation_function(**content)

                self.device_manager.add_device(name, dev_obj, to_file=True)
                self.dialog.items = self.device_manager.get_type_device_list()
                self.dialog.populate_list()
                break
            except Exception as e:
                QMessageBox.critical(dialog, "Error", f"Error adding device: {str(e)}")

    def custom_edit_action(self):
        """
        Custom action to perform when the Edit button is clicked.
        """
        selected_item = self.dialog.get_selected_item()
        if selected_item is not None:
            name = self.device_manager.get_id_from_device_type(selected_item)
            device = self.device_manager.get_device(name)
            if device:
                creation_func = self.device_manager.get_creation_function(
                    device.device_type
                )

                dialog = QDialog()
                layout = QVBoxLayout(dialog)
                form_layout = QFormLayout()

                attrs = list(inspect.signature(creation_func).parameters)
                line_edits = {}
                formatted_attrs = {
                    attr: attr.replace("_", " ").title() for attr in attrs
                }

                for attr in attrs:
                    line_edit = QLineEdit()
                    line_edit.setText(str(device.__dict__[attr]))
                    form_layout.addRow(formatted_attrs[attr], line_edit)
                    line_edits[attr] = line_edit

                layout.addLayout(form_layout)

                button_box = QDialogButtonBox(
                    QDialogButtonBox.Ok | QDialogButtonBox.Cancel
                )
                layout.addWidget(button_box)

                button_box.accepted.connect(dialog.accept)
                button_box.rejected.connect(dialog.reject)

                if dialog.exec_() == QDialog.Accepted:
                    try:
                        kwargs = {attr: line_edits[attr].text() for attr in attrs}
                        device_type = self.device_manager.get_type(name)
                        creation_function = self.device_manager.get_creation_function(
                            device_type
                        )
                        old_obj = self.device_manager.get_device(name)
                        new_obj = creation_func(**kwargs)
                        if old_obj != new_obj:
                            self.device_manager.add_device(
                                name, new_obj, to_file=True, force=True
                            )
                    except Exception as e:
                        QtWidgets.QMessageBox.critical(
                            self.dialog, "Error", f"Error editing device: {str(e)}"
                        )

    def custom_remove_action(self):
        """
        Custom action to perform when the Remove button is clicked.
        """
        selected_item = self.dialog.get_selected_item()
        if selected_item is None:
            return None
        name = self.device_manager.get_id_from_device_type(selected_item)
        reply = QtWidgets.QMessageBox.question(
            self.dialog,
            "Remove Device",
            f"Are you sure you want to remove '{name}'?",
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
            QMessageBox.No,
        )
        if reply == QtWidgets.QMessageBox.Yes:
            try:
                self.device_manager.remove_device(name)
                self.dialog.items = self.device_manager.get_type_device_list()
                self.dialog.populate_list()
            except Exception as e:
                QtWidgets.QMessageBox.critical(
                    self, "Error", f"Error removing device: {str(e)}"
                )

    def run(self):
        self.dialog.exec_()
        self.app.quit()


if __name__ == "__main__":
    app = DeviceManagerApp()
    app.run()
