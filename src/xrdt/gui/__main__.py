from functools import partial
import logging
import os
import sys

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib import pyplot as plt

from PyQt5.QtCore import Qt, QUrl
from PyQt5.QtGui import QDesktopServices
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import (
    QAction,
    QApplication,
    QFrame,
    QInputDialog,
    QMainWindow,
    QMessageBox,
    QVBoxLayout,
)
from PyQt5.uic import loadUiType

from xrdt import __name__ as package_name
from xrdt import __version__ as package_version
from xrdt.plugin_loader import get_plugins
from xrdt.measurement_manager import MeasurementManager
from xrdt.gui.dialogues.device import DeviceManagerApp
from xrdt.gui.dialogues.preset import PresetManagerApp
from xrdt.gui.dialogues.about import AboutDialog

WINDOW_TITLE = f"{package_name} v{package_version} GUI"
URL_DOCS = "https://cs137.codeberg.page/xrdt/"
URL_ISSUES = "https://codeberg.org/Cs137/xrdt/issues"

UI_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "gui.ui")

logger = logging.getLogger(__name__)
ui_main_window, _ = loadUiType(UI_PATH)


class MainWindow(QMainWindow):
    """Main application window."""

    def __init__(
        self,
        measurement_manager: MeasurementManager,
        measurement_id: str = None,
    ):
        super().__init__()
        self._m_manager = measurement_manager
        self._activated_ui_items = []
        self._m_obj = None
        self.presets_app = None
        self.devices_app = None

        # Load the UI file
        self.ui = ui_main_window()
        self.ui.setupUi(self)
        self.setWindowTitle(WINDOW_TITLE)

        # Connect signals to slots
        self.ui.actionOpen.triggered.connect(self.open_measurement)
        self.ui.actionClose.triggered.connect(self.close_measurement)
        self.ui.actionQuit.triggered.connect(self.quit_application)
        self.ui.actionAbout.triggered.connect(self.about)
        self.ui.actionOpen_docs.triggered.connect(self.open_docs)
        self.ui.actionOpen_issues.triggered.connect(self.open_issues)
        self.ui.actionManage_devices.triggered.connect(self.manage_devices)
        self.ui.actionManage_presets.triggered.connect(self.open_preset_manager)

        logging.debug(f"Loaded {package_name} v{package_version} GUI")

        if measurement_id is not None:
            self._m_obj = self.load_measurement(measurement_id)

    @property
    def has_measurement(self) -> bool:
        """Returns True if a measurement is loaded."""
        if self._m_obj is None:
            return False
        return True

    @property
    def measurement_id(self) -> str:
        """Returns ID of loaded measurement or None if no measurement is loaded."""
        if not self.has_measurement:
            return None
        return self._m_obj.meta.measurement_id

    def _reset_qframes(self):
        """Clear the plotFrame and the toolbarFrame layouts."""
        frames = [self.ui.plotFrame, self.ui.toolbarFrame]
        for frame in frames:
            layout = frame.layout()
            if layout is not None:
                while layout.count():
                    item = layout.takeAt(0)
                    if item.widget():
                        item.widget().deleteLater()

    def _set_refiner(self, action: QAction) -> None:
        """Set the refinement interface and update the UI."""
        for item in self.ui.menuSet_refiner.actions():
            item.setChecked(False)
        name = action.text()
        self._m_obj.set_refinement_interface(name)
        action.setChecked(True)
        self.ui.actionRefine.setEnabled(True)

    def _activate_refinement_menu(self) -> None:
        """Activate the refinement menu and add available refiners."""
        sub_menu = self.ui.menuSet_refiner
        existing_refiners = [a.text() for a in sub_menu.actions()]
        refiners = get_plugins("refinement")
        for refiner in refiners:
            if refiner not in existing_refiners:
                action = QAction(refiner, self)
                action.setCheckable(True)
                sub_menu.addAction(action)
                function = partial(self._set_refiner, action=action)
                action.triggered.connect(function)
        if len(refiners) > 0:
            self._activate_ui_item(self.ui.menuSet_refiner)
            self._activate_ui_item(self.ui.actionRefine)

        # Set refiner if only one exists
        if len(refiners) == 1:
            selected_refiner = [
                a for a in sub_menu.actions() if a.text() == refiners[0]
            ]
            self._set_refiner(*selected_refiner)

    def _activate_ui_item(self, item, checked=False) -> None:
        """Activate a UI item and add it to the activated items list."""
        item.setEnabled(True)
        self._activated_ui_items.append(item)
        logging.debug(f"Activated UI item {item!r}")
        if checked:
            item.setChecked(True)

    def _deactivate_ui_item(self, item) -> None:
        """Deactivate a UI item."""
        item.setEnabled(False)
        logging.debug(f"Deactivated UI item {item!r}")

    def _deactivate_ui_items(self) -> None:
        """Deactivate all activated UI items."""
        for item in self._activated_ui_items:
            self._deactivate_ui_item(item)

    def _plot_measurement(self) -> None:
        """Plot the measurement data on the canvas."""
        self._reset_qframes()
        frames = [self.ui.plotFrame, self.ui.toolbarFrame]
        layouts = []
        for frame in frames:
            # Create a new layout for the QFrame if it doesn't have one
            if frame.layout() is None:
                layout = QVBoxLayout(frame)
                frame.setLayout(layout)
            else:
                layout = frame.layout()
            layouts.append(layout)

        figure = Figure(tight_layout=True)
        canvas = FigureCanvas(figure)
        toolbar = NavigationToolbar(canvas, self)

        # Add the toolbar and canvas to the layout
        layouts[1].addWidget(toolbar)
        layouts[0].addWidget(canvas)

        self._m_obj.plot(norm=self.ui.checkBox_norm.isChecked(), figure=figure)
        logging.debug(f"Plotted {self.measurement_id!r}")

    def close_measurement(self) -> None:
        """Close a opened measurement."""
        if self.has_measurement:
            logging.info(f"Closing measurement {self.measurement_id!r}...")
            self._m_obj = None
            self.setWindowTitle(WINDOW_TITLE)
            self.ui.labelStatus.setText(None)
            self._reset_qframes()
            self._deactivate_ui_items()

    def load_measurement(self, measurement_id) -> None:
        """Load a measurement object from the measurement manager and reinitialize the UI."""
        logging.debug(f"Loading measurement {self.measurement_id!r}...")
        self._m_obj = self._m_manager.get_measurement(measurement_id)

        # Initialise UI items
        self.setWindowTitle(
            f"{WINDOW_TITLE} | {self.measurement_id} ({self._m_obj.meta.sample})"
        )
        to_activate = [
            self.ui.menuMeasurement,
            self.ui.checkBox_norm,
            self.ui.labelNorm_title,
            self.ui.actionClose,
        ]
        for item in to_activate:
            self._activate_ui_item(item)
        self._activate_refinement_menu()
        processing_state = self._m_obj.get_processing_state()
        self.ui.labelStatus.setText(processing_state)
        if processing_state is not None:
            self._activate_ui_item(self.ui.labelStatus_title)
        self._plot_measurement()

        # Connect actions
        self.ui.checkBox_norm.clicked.connect(self._plot_measurement)
        self.ui.actionRefine.triggered.connect(self.refine)

    def open_measurement(self) -> None:
        """Open a measurement and load its object from the measurement manager."""
        measurements = self._m_manager.list_measurements()
        selected_measurement, ok = QInputDialog.getItem(
            None,
            "Select measurement",
            "Select a measurement:",
            measurements,
            editable=False,
        )
        if ok:
            if self.has_measurement:
                self.close_measurement()
            self.load_measurement(selected_measurement)

    def refine(self):
        """Perform the refinement process on the measurement."""
        self._m_obj.refine()

    def open_docs(self):
        """Open the documentation URL in a web browser."""
        QDesktopServices.openUrl(QUrl(URL_DOCS))

    def open_issues(self):
        """Open the issues URL in a web browser."""
        QDesktopServices.openUrl(QUrl(URL_ISSUES))

    def about(self):
        """Show the About dialog."""
        dialog = AboutDialog()
        dialog.exec_()

    def manage_devices(self):
        """Open the device manager GUI."""
        if self.devices_app is None:
            self.devices_app = DeviceManagerApp()
            self.devices_app.dialog.setWindowFlags(Qt.Dialog)
        self.devices_app.dialog.show()

    def open_preset_manager(self):
        """Open the preset manager GUI."""
        if self.presets_app is None:
            self.presets_app = PresetManagerApp()
            self.presets_app.dialog.setWindowFlags(Qt.Dialog)
        self.presets_app.dialog.show()

    def closeEvent(self, event):
        """Override the closeEvent method to show a confirmation dialog when closing the main window."""
        logging.info(f"Quitting {package_name}...")
        confirm = QMessageBox.question(
            self,
            "Confirm close",
            f"<p>Are you sure you want to close {package_name}?</p>",
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No,
        )
        if confirm == QMessageBox.Yes:
            logging.debug(f"Quitting {package_name} confirmed")
        else:
            event.ignore()
            logging.debug(f"Quitting {package_name} aborted")

    def quit_application(self):
        """Quit the application."""
        self.close()


def main():
    """Entry point of the application."""
    # Create the application
    app = QApplication(sys.argv)

    # Set up logging
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
    )

    # Create the measurement manager
    measurement_manager = MeasurementManager()

    # Create the main window
    window = MainWindow(measurement_manager)

    # Show the main window
    window.show()

    # Execute the application
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
