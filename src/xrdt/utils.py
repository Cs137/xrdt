""""Helper functions for xrd-tools."""
import json
import logging
import os

from .config import ENCODING, JSON_INDENT

logger = logging.getLogger(__name__)


def ensure_file_exists(file_path) -> None:
    """Raise a FileNotFoundError if file_path does not exist."""
    if not os.path.isfile(file_path):
        raise FileNotFoundError(
            f"The file '{os.path.basename(file_path)}' could not be found."
        )


def make_dirs(file_path) -> None:
    """Create parent dirs for file path if not existing."""
    directory = os.path.dirname(file_path)
    if not os.path.isdir(directory):
        os.makedirs(directory, exist_ok=False)
        logger.debug(f"Created directory: '{directory}'")


def raise_file_exists(file_path, force=False) -> None:
    """Check if the file already exists and raise a FileExistsError if so.

    The error message will include instructions on how to overwrite the file
    if the 'force' flag is set to True (default).

    Parameters:
        file_path (str): The file path to check.
        force (bool, optional): Whether to include instructions on how to
            overwrite the file in the error message using a "force" flag.

    Raises:
        FileExistsError: If the specified file already exists.
    """
    if os.path.isfile(file_path):
        msg = f"File {file_path!r} already exists."
        if force:
            msg += " Use the 'force' flag to overwrite it."
        raise FileExistsError(msg)


def write_to_json(
    file_path: str, data: any, indent: int = JSON_INDENT, encoding: str = ENCODING
) -> None:
    """Write data to JSON file."""
    json_str = json.dumps(data, indent=indent)
    print(json_str, file=open(file_path, "wt", encoding=encoding))
    logger.debug(f"Written to file: {file_path!r}.")
