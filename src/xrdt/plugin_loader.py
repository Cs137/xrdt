"""A simple plugin loader."""
import importlib
import logging
import os

from . import __name__ as PACKAGE_NAME
from . import refinement_interface_factory

PLUGIN_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "plugins"))

logger = logging.getLogger(__name__)


class ModuleInterface:
    """Represents a xrd-tools plugin interface.

    A plugin has a register function, as well as a plugin_type and a plugin_name.
    The latter have to represent the plugin module filename as follows:
    <plugin_type>_<plugin_name>.py

    Moreover, the module has to be stored in the 'plugins' directory of this package.
    """

    @staticmethod
    def register() -> None:
        """
        Function to register the plugin at the factory for the corresponding plugin type.
        """


def get_plugins(plugin_type: str) -> list[str]:
    """Get names for plugins of a certain type.

    The plugin file has to be named as follows: <plugin_type>_<plugin_name>.py, and
    it has to be located in the 'plugins' directory within this package.

    Returns:
        list[str]: List of plugin names for provided plugin type.
    """
    return [
        item.name.replace(f"{plugin_type}_", "").replace(".py", "")
        for item in os.scandir(PLUGIN_DIR)
        if item.name.startswith(plugin_type)
    ]


def import_module(name: str) -> ModuleInterface:
    """Imports a module given a name."""
    return importlib.import_module(name)  # type: ignore


def load_plugins(plugin_type: str) -> None:
    """Loads all plugins of a certain plugin type."""
    logger.debug(f"Loading {plugin_type} plugins...")
    for name in get_plugins(plugin_type):
        module = f"{PACKAGE_NAME}.plugins.{plugin_type}_{name}"
        logger.debug(f"Loading plugin {name!r} from module {module!r}...")
        plugin = import_module(module)
        logger.debug(f"Loaded {plugin_type} plugin {name!r}.")
        plugin.register()
