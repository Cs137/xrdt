from dataclasses import dataclass

from .config import DEVICE_TYPES


@dataclass
class BaseDevice:
    """Basic device class."""

    manufacturer: str
    model: str
    inventory_no: str = None

    def quotation(self, format_type: str = None) -> str:
        """Retrieve a descriptive string to be used in reports.

        A `format_type` can be defined in order to obtain a formatted string.
        Currently `latex` and `md` are supported `format_type` strings. The
        latter results e.g. in the following output:
        `*X'Pert Pro* manufactured by *PANanalytical*`
        """
        if format_type == "md":
            return f"*{self.model}* manufactured by *{self.manufacturer}*"
        if format_type == "latex":
            return f"\textit{{{self.model}}} manufactured by \textit{{{self.manufacturer}}}"
        return f"{self.model} manufactured by {self.manufacturer}"


@dataclass
class LabDiffractometer(BaseDevice):
    """XRD device."""

    @property
    def device_type(self) -> str:
        """Returns string with the type of device."""
        return DEVICE_TYPES["lab_xrd"]


@dataclass
class HTChamber(BaseDevice):
    """High-temperature chamber."""

    @property
    def device_type(self) -> str:
        """Returns string with the type of device."""
        return DEVICE_TYPES["ht_chamber"]
