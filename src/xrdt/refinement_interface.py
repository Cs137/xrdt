"""Refinement interface module."""
from abc import ABC

from .refinement import RefinementResult
import pandas as pd


class AppNotInstalledError(Exception):
    """Error that is raised if the refinement application is not installed."""


class RefinementInterface(ABC):
    """Basic representation of a refinement interface.

    A refinement interface has the property 'file_refinement_input', and the methods
    'create_refinement_input', 'get_refinement_properties', 'get_refinement_results'
    and 'open_refinement'. The latter checks for the required application and raise
    an 'AppNotInstalledError' if the application is not installed on the machine.

    It has to accept the arguments listed below.

    Args:
        measurement_id (str): ID of the measurement to be loaded to the plugin.
        data (pd.Series): Series containing the x/y data of the measurement.
            The index represents the 2theta angle.
        project_dir (str): Path to the refinement project directory.
        encoding (str): Encoding used in data file.
    """

    measurement_id: str
    data: pd.Series
    dir_refinement: str
    encoding: str

    def create_input_data(self) -> None:
        """Create refinement input data for refinement application."""

    def get_cif_files(self) -> dict[str, str]:
        """Get a dictionary with the name and the path to the cif files for refined phase(s)."""

    def get_refined_data(self) -> pd.DataFrame:
        """Get a pandas DataFrame containing the refined data series.

        Args:
            i_calc (str): Column name for the calculated intensities.
            i_bg (str): Column name for the background intensities.

        Returns:
            pd.DataFrame: A DataFrame with index set to 2θ and an index name
            as found in the provided data. The columns correspond to:
            - I_calc with column name provided as argument,
            - I_bg with column name provided as argument, and
            - a further column for each phase refined, named with its name as defined
              in profex.
        """

    def get_refinement_result(self) -> RefinementResult:
        """Get a RefinementResults object for the refinement of the measurement."""

    def open_refinement(self) -> None:
        """Open refinement project with refinement application.

        Raises:
            AppNotInstalledError: If the refinement applicaiton is not installed.
        """
