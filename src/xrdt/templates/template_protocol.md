---

title:      Protocol for XRD measurement {{ measurement_id }}
author:     {{ author }}
date:       {{ date }}

sample:     {{ sample }}

---

## Measurement details

[Measurement details]

## Notes

[Additional notes]
