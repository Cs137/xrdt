import os

from environs import Env

APPNAME = "xrdt"
_HOME = os.path.expanduser("~")
_SHARE = os.path.join(os.path.expanduser("~/.local/share"), APPNAME)

env = Env()

# Read values from .env file if it exsists
env.read_env(".env.docs", recurse=False, override=True)

# Assign env variable- or default- values
AUTHOR = env("SCI_AUTHOR")
EDITOR = env("EDITOR", "nano")


with env.prefixed("XRDT_"):
    DATABASE = env("DB", os.path.join(_SHARE, "measurements.csv"))
    DEVICES = env("DEVICES", os.path.join(_SHARE, "devices.json"))
    LOG_DIR = env("LOG_DIR", os.path.join(_SHARE, "logs"))
    MEASUREMENTS_DIR = env("MEASUREMENTS_DIR", os.path.join(_HOME, "XRD_measurements"))
    PRESETS = env("PRESETS", os.path.join(_SHARE, "presets.json"))
    TEMPLATE_DIR = env("TEMPLATE_DIR", os.path.join(_SHARE, "templates"))


#######################################
# Fixed settings

ENCODING = "utf-8"
JSON_INDENT = 4
ZIP_COMPRESS_LEVEL = 9

PROCESSING_STATES = {
    "refined": "refined",
}

MEASUREMENT_SUBDIRS = {
    "data": "data",
    "refinement": "refinement",
    "results": "results",
}

FILE_SUFFIXES = {
    "meta": "_meta",
    "data": "_data",
    "refined": "_refined",
    "source_data": "_source-data",
    "protocol": "_protocol",
}

XRD_DATA_COLUMNS = {
    "angle": "2θ / °",
    "int_abs": "Intensity / counts",
    "int_norm": "Normalised intensity",
    "int_obs": "I_observed",
    "int_calc": "I_calculated",
    "int_bg": "I_background",
}

DEVICE_TYPES = {
    "lab_xrd": "X-ray diffractometer",
    "ht_chamber": "High-temperature chamber",
}
