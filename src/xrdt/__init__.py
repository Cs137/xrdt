import logging
import os
from configparser import ConfigParser

from .config import APPNAME, LOG_DIR


def get_package_version():
    """Reads the version number from pyproject.toml."""
    # Get the path to pyproject.toml
    pyproject_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "../..", "pyproject.toml")
    )

    # Read the version number using ConfigParser
    config = ConfigParser()
    config.read(pyproject_path)
    return config.get("tool.poetry", "version").strip('"')


def configure_logging():
    """Configures the logging settings."""
    log_file = os.path.join(LOG_DIR, APPNAME + ".log")
    file_level = logging.DEBUG
    file_formatter = logging.Formatter(
        "%(asctime)s : %(levelname)s : %(name)s : %(message)s"
    )
    file_handler = logging.FileHandler(log_file, "a")
    file_handler.setLevel(file_level)
    file_handler.setFormatter(file_formatter)

    stream_level = logging.INFO
    stream_formatter = logging.Formatter("%(message)s")
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(stream_level)
    stream_handler.setFormatter(stream_formatter)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger


def initialise_logger():
    """Initializes the logger based on the existence of the log directory."""
    if not os.path.isdir(LOG_DIR):
        # Create the log directory if it doesn't exist
        os.makedirs(LOG_DIR, exist_ok=False)
        logger = configure_logging()
        logger.info(f"Created logging directory: '{LOG_DIR}'")
    else:
        # Log directory already exists, initialize the logger
        logger = configure_logging()
    return logger


# Get the package version from pyproject.toml
__version__ = get_package_version()

# Initialize the logger
logger = initialise_logger()
