import os
from dataclasses import dataclass

from .config import FILE_SUFFIXES, MEASUREMENT_SUBDIRS


@dataclass
class MeasurementPaths:
    """
    The `MeasurementPaths` class provides file- and directory-paths relevant for
    a XRD measurement and is used by other classes and functions of this package.

    Parameter:
        :measurements_dir:          Path to the measurements directory
        :measurement_id:            ID of the measurement
        :subdir_data:               Subdirectory for data
        :subdir_refined:            Subdirectory for refined data
        :subdir_results:            Subdirectory for result
        :suffix_data:               Suffix for scan data file
        :suffix_meta:               Suffix for meta data file
        :suffix_protocol:           Suffix for measurement protocol file
        :suffix_refined_data:       Suffix for refined data file
        :suffix_refinement_results: Suffix for refinement results file
        :suffix_source_data:        Suffix for source data file

    """

    measurements_dir: str
    measurement_id: str
    subdir_data: str = MEASUREMENT_SUBDIRS["data"]
    subdir_refined: str = MEASUREMENT_SUBDIRS["refinement"]
    subdir_results: str = MEASUREMENT_SUBDIRS["results"]
    suffix_data: str = FILE_SUFFIXES["data"]
    suffix_meta: str = FILE_SUFFIXES["meta"]
    suffix_protocol: str = FILE_SUFFIXES["protocol"]
    suffix_refined_data: str = FILE_SUFFIXES["refined"]
    suffix_refinement_results: str = FILE_SUFFIXES["refined"]
    suffix_source_data: str = FILE_SUFFIXES["source_data"]

    @property
    def dir_data(self) -> str:
        """Returns path to data directory."""
        return os.path.join(
            self.measurements_dir,
            self.measurement_id,
            self.subdir_data,
        )

    @property
    def dir_refinement(self) -> str:
        """Returns path to refinement directory."""
        return os.path.join(
            self.dir_data,
            self.subdir_refined,
        )

    @property
    def dir_results(self) -> str:
        """Returns path to results directory."""
        return os.path.join(
            self.measurements_dir,
            self.measurement_id,
            self.subdir_results,
        )

    @property
    def file_data(self) -> str:
        """Returns path to data file (``.csv``)."""
        return os.path.join(
            self.dir_data,
            self.measurement_id + self.suffix_data + ".csv",
        )

    @property
    def file_log(self) -> str:
        """Returns path to log file (``.log``)."""
        return os.path.join(
            self.measurements_dir,
            self.measurement_id,
            self.measurement_id + ".log",
        )

    @property
    def file_meta(self) -> str:
        """Returns path to meta file (``.json``)."""
        return os.path.join(
            self.dir_data,
            self.measurement_id + self.suffix_meta + ".json",
        )

    @property
    def file_protocol(self) -> str:
        """Returns path to protocol file (``.md``)."""
        return os.path.join(
            self.measurements_dir,
            self.measurement_id,
            self.measurement_id + self.suffix_protocol + ".md",
        )

    @property
    def file_refined_data(self) -> str:
        """Returns path to refined data file (``.md``)."""
        return os.path.join(
            self.dir_data,
            self.measurement_id + self.suffix_refined_data + ".csv",
        )

    @property
    def file_refinement_result(self) -> str:
        """Returns path to refinement results file (``.json``)."""
        return os.path.join(
            self.dir_data,
            self.measurement_id + self.suffix_refinement_results + ".json",
        )

    @property
    def file_source_data(self) -> str:
        """Returns path to source archive file (``.zip``)."""
        return os.path.join(
            self.dir_data,
            self.measurement_id + self.suffix_source_data + ".zip",
        )

    def get_cif_file_path(self, phase: str) -> str:
        """Return path to a cif file for a specified phase (``.cif``)."""
        return os.path.join(
            self.dir_results,
            self.measurement_id + self.suffix_refined_data + "_" + phase + ".cif",
        )
