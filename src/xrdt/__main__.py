"""xrd-tools main CLI module, launches the CLI application."""
import logging
import os

import click

from . import __version__, utils
from . import __name__ as package_name
from .config import MEASUREMENTS_DIR
from .measurement_manager import MeasurementManager
from .cli.cli_preset import preset
from .cli.cli_device import device
from .cli.cli_template import template
from .cli.cli_measurement import measurement
from .cli.cli_database import database
from .cli.cli_gui import gui

logger = logging.getLogger(__name__)
pass_measurement_manager = click.make_pass_decorator(MeasurementManager)


@click.group
@click.option(
    "--measurements-dir",
    type=click.Path(),
    required=True,
    default=MEASUREMENTS_DIR,
    show_default=True,
    help="Path to the measurements directory.",
)
@click.option(
    "--log-level",
    type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]),
    default="INFO",
    show_default=True,
    help="Set the log level",
)
@click.version_option(version=__version__)
@click.pass_context
def cli(ctx, measurements_dir: str, log_level):
    """The main command to start the CLI provided by xrd-tools.

    It provides a subset of commands for individual tasks to manage
    data related to XRD measurements.
    """
    logging.basicConfig(level=log_level)
    logging.info(f"Running {package_name} v{__version__}")

    # Initialise a measurement manager instance
    ctx.obj = MeasurementManager(measurements_dir=measurements_dir)


cli.add_command(database)
cli.add_command(device)
cli.add_command(measurement)
cli.add_command(preset)
cli.add_command(template)
cli.add_command(gui)


# HOUSEKEEPING #############################################

# @cli.group()
# @click.pass_context
# def housekeeping():
#     """CLI to manage xrd-tools."""


# @database.command("create")
# @pass_measurement_db
# def compare_measurements(measurement_database: MeasurementDatabase):
#     """Compare measurements registered in the DB."""
#     options = list(measurement_database.entries.index)
#     terminal_menu = TerminalMenu(
#         options,
#         title="Select measurements to compare:",
#         multi_select=True,
#         show_multi_select_hint=True,
#     )
#     menu_entry_indices = terminal_menu.show()
#     ids = [options[i] for i in menu_entry_indices]
#     measurements = [measurement_database._m_manager.get_measurement(i) for i in ids]
#     am = AnalyseMeasurements(measurements)
#     am.multiplot()


############################################################


def main():
    cli()


if __name__ == "__main__":
    main()
