from typing import Callable, Protocol


class Device(Protocol):
    """Basic representation of a device."""

    model: str
    manufacturer: str

    @property
    def device_type(self) -> str:
        ...


# Dictionary to track which device type is initiated by which class
device_creation_funcs: dict[str, Callable[..., Device]] = {}


def register(device_type: str, creation_func: Callable[..., Device]) -> None:
    """Register a new device type."""
    device_creation_funcs[device_type] = creation_func


def unregister(device_type: str) -> None:
    """Unregister a device type."""
    device_creation_funcs.pop(device_type, None)


def create(arguments: dict[str, any]) -> tuple[Device, str]:
    """Create a device of a specific type, given a dictionary of arguments."""
    args_copy = arguments.copy()
    device_type = args_copy.pop("device_type")
    if "device_id" in args_copy.keys():
        device_id = args_copy.pop("device_id")
    else:
        device_id = None
    try:
        creation_func = device_creation_funcs[device_type]
        return creation_func(**args_copy), device_id
    except KeyError:
        raise ValueError(f"Unknown device type {device_type!r}") from None
