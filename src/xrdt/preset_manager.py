"""Module providing tools to handle preset dictionaries, which are stored in a JSON file."""

import json
import logging
import os
from dataclasses import dataclass

from .config import ENCODING, JSON_INDENT

logger = logging.getLogger(__name__)


@dataclass
class PresetManager:
    """Manager to handle preset dictionaries, which are stored in a JSON file.

    Args:
        file_path (str): Path to a JSON file containing preset dictionaries.
        encoding (str): Encoding applied when writing to and reading from JSON files.
        indent (int): Indent applied when writing to a JSON file.
    """

    file_path: str
    encoding: str = ENCODING
    indent: int = JSON_INDENT

    def __post_init__(self):
        # Create dict with preset name (k) and preset key/value pairs (v)
        data = self._load_data()
        self._preset_dict = {dct.pop("name"): dct for dct in data}

    def _load_data(self) -> list[dict[str, any]]:
        """Read preset dictionaries from the JSON file."""
        if not os.path.isfile(self.file_path):
            return []
        with open(self.file_path, "r", encoding=self.encoding) as fobj:
            json_str = fobj.read()
        logger.debug(f"Loaded presets from: '{os.path.abspath(self.file_path)}'")
        return json.loads(json_str)

    def _write_data(self) -> None:
        """Write presets to the JSON file."""
        # Get data
        data = []
        for k, v in self._preset_dict.items():
            dct = {"name": k}
            dct.update(v)
            data.append(dct)
        # Ensure parent directory exists
        directory = os.path.dirname(self.file_path)
        if not os.path.isdir(directory):
            os.makedirs(directory, exist_ok=False)
            logger.debug(f"Created preset directory: '{directory}'")
        # Write data (it's printed to the file in order to maintain the indent)
        json_str = json.dumps(data, indent=self.indent)
        print(json_str, file=open(self.file_path, "wt", encoding=self.encoding))
        logger.debug(f"Preset data written to: {self.file_path}")

    @property
    def has_preset(self) -> bool:
        """Check if registered presets are existing.

        Returns:
            bool: True if at least one preset is registered,
            and False if no preset is registered.
        """
        if len(self.get_names()) == 0:
            return False
        return True

    def get_names(self) -> list[str]:
        """Retrieve a list with names of registered presets.

        Returns:
            list[str]: List containg the names of all registered presets.
        """
        return list(self._preset_dict.keys())

    def get_preset(self, name: str) -> dict[str, any]:
        """Retrieve a preset dictionary.

        Args:
            name (str): The name of the preset to be returned.
        Returns:
            dict[str, any]: Dictionary containg the preset key/value pairs.
        """
        return self._preset_dict[name]

    def add_preset(
        self, name: str, preset_dct: dict[str, any], to_file: bool = True
    ) -> None:
        """Add a new preset (and write to JSON file).

        Args:
            name (str): The name of the preset to be added.
            preset_dct (str): Dictionary containg the preset key/value pairs to be added.
            to_file (bool): Flag to indicate to write the new presets to the JSON file.
        Raises:
            ValueError: If the preset name is already registered.
        """
        if name in self.get_names():
            raise ValueError(f"Preset with name {name!r} already existing.")
        self._preset_dict[name] = preset_dct
        logger.info(f"Added preset: {name!r}")
        if to_file:
            self._write_data()

    def remove_preset(self, name: str, to_file: bool = True) -> None:
        """Remove a registered preset (and write to JSON file).

        Args:
            name (str): The name of the preset to be removed.
            to_file (bool): Flag to indicate to write the new presets to the JSON file.
        """
        if name not in self.get_names():
            raise ValueError(f"Preset with name {name!r} not existing.")
        self._preset_dict.pop(name)
        logger.info(f"Removed preset: {name!r}")
        if to_file:
            if not self.has_preset:
                os.remove(self.file_path)
                logger.debug(f"Deleted empty preset file: {self.file_path!r}")
            else:
                self._write_data()
