""" Module that provided metadata related helper functions for the cli of xrd-tools. """
import inspect
import logging
import os
import sys

import click

from . import cli_utils

sys.path.insert(0, os.path.abspath(".."))
from ..config import (
    DEVICES,
    PRESETS,
    TEMPLATE_DIR,
    MEASUREMENTS_DIR,
)
from ..device_manager import DeviceManager
from ..measurement import Measurement
from ..meta import Meta
from ..paths import MeasurementPaths
from ..templates import TemplateManager, get_template_cli

logger = logging.getLogger(__name__)


def assigne_device_cli(
    dmanager: DeviceManager, device_type: str, device_id: str = None
) -> dict[str, any]:
    """Create device arguments for a certain device type based on user input.

    Args:
        device_manager (DeviceManager): DeviceManager instance initiated with
            device_file for devices of interest.
        device_type (str): Valid device type
        device_id (str, optional): Valid device ID, no user resonse required.
    Returns:
        dict[str, any]: Dictionary containing keyword arguments to initialise
            an instance of the corresponding device object class.
    """
    if os.path.isfile(dmanager.file_path):
        options = dmanager.get_device_ids(device_type)
        if len(options) == 0:
            logger.debug(f"No {device_type} registered.")
        elif len(options) == 1:
            device_id = options[0]
        else:
            msg = f"Select a {device_type}:"
            device_id = cli_utils.select_option(options, msg)
    if device_id is not None:
        dev_obj = dmanager.get_device(device_id)
        dev_dct = {"device_type": dev_obj.device_type}
        dev_dct.update(dev_obj.__dict__.copy())
        dev_dct = {k: v for k, v in dev_dct.items() if v is not None}
        logger.info(f"Assigned {device_type}: {device_id}")
        return dev_dct


def get_ht_mode_cli(options=["XRD (ambient)", "HT-XRD"]) -> bool:
    """Get ht_mode from the use.

    Args:
        options (lst[str]): List with modes to be asked for, it is limited
            to two items and the last one has to be the one for `ht_mode=True`.
    Returns:
        bool: True if user selected "HT XRD", False if "ambient XRD" was select_option.
    """
    msg = "Select measurement mode:"
    ht_mode = bool(cli_utils.select_option(options, msg, return_index=True))
    logger.debug(f"Measurement mode: {options[int(ht_mode)]!r}")
    return ht_mode


def get_meta_arg_types(cls=Meta) -> dict[str, str]:
    """Get the names and types of (keyword) arguments of a certain class as dict.

    The (keyword) argument types are extracted from the type hints of the class.

    :param cls: the class to get (keyword) arguments and types for.

    Returns:
        dict[str, any]: Dictionary with class (keyword) arguments as keys and the
        corresponding type as values.
    """
    # Get the signature of the class
    signature = inspect.signature(cls)

    # Iterate over the arguments of the Meta class
    types = {}
    for name, param in signature.parameters.items():
        # Check if the annotation is a class
        if inspect.isclass(param.annotation):
            # Use the corresponding built-in type
            if param.annotation is str:
                type_ = str
            elif param.annotation is int:
                type_ = int
            elif param.annotation is float:
                type_ = float
            elif param.annotation is bool:
                type_ = bool
            else:
                # Default to str if the annotation is not a built-in type
                type_ = str
        else:
            # Use the annotation as the type if it is not a class
            type_ = param.annotation

        types[name] = type_

    return types


def get_meta_kwargs(
    meta_dict: dict[str, any] = {},
    device_file: str = None,
    create_preset=False,
) -> dict[str, any]:
    """Get a dictionary with keyword arguments to initiate a metadata instance.

    The user is prompted to provide values for each (keyword) argument.
    The function is used in order to create a measurement preset of a new measurement.

    :param meta_dict: dictionary containg Meta keyword arguments (e.g. preset
        values). If provided, the values are taken and the user is not asked to
        provide a value for this (keyword) argument.
    :param device_file: file path to the JSON device file.
    :param create_preset: flag to indicate whether the function is called to create
        a preset. If so, the prompt for measurement ID assignment is omitted and the
        the default definition prompt is set to [Y], otherwise it is set to [N].

    Returns:
        dict[str, any]: Dictionary with Meta (keyword) arguments as keys and the corresponding
        values. The dictionary can be used to initialise a Meta instance.
    """
    # Get the Meta keywords and types
    types = get_meta_arg_types()

    # Iterate over the arguments and prompt for input
    ignore_for_preset_creation = ["measurement_id", "sample", "xrd_datetime"]
    for arg, type_ in types.items():
        if arg not in meta_dict.keys():
            item = arg.replace("_", " ")
            if arg in ignore_for_preset_creation and create_preset:
                pass
            elif arg == "measurement_id" and not create_preset:
                meta_dict[arg] = click.prompt(
                    "Enter measurement ID", type=type_
                ).strip()
            elif arg == "ht_mode":
                # should always be predefined for measurement addition
                meta_dict[arg] = get_ht_mode_cli()
            elif click.confirm(
                f" > Define {item}", default=create_preset, prompt_suffix="? "
            ):
                if arg == "operator" and os.environ.get("SCI_AUTHOR") is not None:
                    meta_dict[arg] = click.prompt(
                        "Enter value",
                        type=type_,
                        default=os.environ.get("SCI_AUTHOR"),
                    )
                elif arg == "devices":
                    dmanager = DeviceManager(device_file)
                    devices = []
                    devices.append(
                        assigne_device_cli(
                            dmanager, device_type=dmanager.device_types["lab_xrd"]
                        )
                    )
                    # Note: The check below works only if the ht_mode argument is defined
                    # prior to the device argument in the definition of the Meta class!
                    if meta_dict["ht_mode"]:
                        devices.append(
                            assigne_device_cli(
                                dmanager,
                                device_type=dmanager.device_types["ht_chamber"],
                            )
                        )
                    meta_dict[arg] = devices
                else:
                    meta_dict[arg] = click.prompt("Enter value", type=type_)

    # Return not None kword arguments as dict
    return {k: v for k, v in meta_dict.items() if v is not None}


def create_meta_obj(
    meta_dict: dict[str, any] = {},
    device_file: str = DEVICES,
    file_path: str = None,
    to_file: bool = True,
    force: bool = False,
    measurements_dir: str = MEASUREMENTS_DIR,
) -> Meta:
    """
    Create a new metadata instance, the user is prompted to provide values.

    :param meta_dict: dictionary containg Meta keyword arguments (e.g. preset values).
    :param device_file: file path to the JSON device file.
    :param file_path: file path to write the Meta instance to as a JSON file.
    :param to_file: flag to indicate whether to write the Meta instance to a JSON file.
    :param force: flag to indicate to overwrite existing file without confirmation.
    :return: Meta instance with attributes initialized from the user input.
    """
    # Get dictionary with arguments and corresponding using user input
    kwargs = get_meta_kwargs(
        meta_dict=meta_dict, device_file=device_file, create_preset=False
    )

    # Create a new Meta instance using the user input as keyword arguments
    meta_obj = Meta(**kwargs)
    logger.info(f"Created metadata for measurement {meta_obj.measurement_id!r}.")

    if to_file:
        # Get file path from MeasurementPaths object if not provided
        if file_path is None:
            paths = MeasurementPaths(measurements_dir, meta_obj.measurement_id)
            file_path = paths.file_meta

        # Confirm to replace existing measurement if force flag is not set
        if not force:
            cli_utils.confirm_file_replacement(file_path)

        # Create measurement data directory if not existing
        cli_utils.make_dirs(file_path)

        # Write the Meta instance to a JSON file
        meta_obj.to_json(file_path)

    return meta_obj


def create_protocol_cli(
    measurement: Measurement,
    author: str = None,
    template_name: str = None,
    template_dir: str = TEMPLATE_DIR,
) -> None:
    """Create a measurement protocol, select a template if multiple options exist."""
    template_manager = TemplateManager(template_dir)
    template_name, template_file = get_template_cli(
        template_manager, "protocol", template_name
    )

    # Define author
    if author is None:
        author = click.prompt("Enter the author's name").strip()

    logger.debug(
        f"Creating protocol for measurement {measurement.meta.measurement_id!r}..."
    )
    logger.debug(f"Using {template_name!r} protocol template.")
    measurement.create_protocol(author, template_file)
