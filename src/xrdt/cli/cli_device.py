""" Module that provided the ``device`` CLI command for xrd-tools. """
import inspect
import logging
import os
import sys

import click

from . import cli_utils

sys.path.insert(0, os.path.abspath(".."))
from ..device_manager import DeviceManager
from ..config import (
    DEVICES,
    DEVICE_TYPES,
    EDITOR,
)

logger = logging.getLogger(__name__)
pass_device_manager = click.make_pass_decorator(DeviceManager)


@click.group()
@click.option(
    "--devices",
    type=click.Path(),
    default=DEVICES,
    required=True,
    show_default=True,
    help="Path to the device file (JSON).",
)
@click.pass_context
def device(ctx, devices: str):
    """CLI to manage measurement devices."""
    ctx.obj = DeviceManager(file_path=devices)


@device.command("add")
@click.option(
    "--name",
    type=str,
    required=True,
    prompt="Enter a name for the new device",
    help="Name of device to be added.",
)
@click.option(
    "--device-type", type=click.Choice(DEVICE_TYPES.values()), help="Device type."
)
@pass_device_manager
def add_device(device_manager: DeviceManager, name: str, device_type: str):
    "Create and register a new device." ""
    if device_type is None:
        msg = "Select a type of device to be added:"
        options = list(device_manager.device_types.values())
        device_type = cli_utils.select_option(options, msg)
    elif device_type not in device_manager.device_types.values():
        raise ValueError(f"Unknown device type declared ({device_type!r}).")
    logger.debug(f"Creating {device_type} device...")
    creation_func = device_manager.get_creation_function(device_type)
    attrs = list(inspect.signature(creation_func).parameters)
    kwargs = {attr: click.prompt(f" > Define {attr}").strip() for attr in attrs}
    kwargs = {k: v if v != "" else None for k, v in kwargs.items()}
    dev_obj = creation_func(**kwargs)
    device_manager.add_device(name, dev_obj, to_file=True)


@device.command("remove")
@click.option(
    "--name",
    type=str,
    help="Name of device to be removed.",
)
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Skip the confirmation prompt.",
)
@pass_device_manager
def remove_device(
    device_manager: DeviceManager, name: str = None, force: bool = False
) -> None:
    """Remove a registered device."""
    choices = device_manager.get_type_device_list()
    if len(choices) == 0:
        logger.debug("No device registered.")
        return None
    if name is None:
        msg = "Select a device to be removed:"
        selection = cli_utils.select_option(choices, msg)
        _, name = selection.split(device_manager.separator)
    if not force:
        click.confirm(
            f"Are you sure you want to remove '{name}'",
            abort=True,
            prompt_suffix="? ",
        )
    device_manager.remove_device(name)


@device.command("list")
@pass_device_manager
def list_devices(device_manager: DeviceManager):
    "List all registered devices." ""
    devices = device_manager.get_type_device_list()
    if len(devices) == 0:
        return None
    click.echo("Registered devices:")
    for device in devices:
        click.echo(f" * {device}")


@device.command("edit")
@click.option(
    "--editor",
    type=str,
    default=EDITOR,
    show_default=True,
    help="Editor to be used.",
)
@pass_device_manager
def edit_devices(device_manager: DeviceManager, editor: str) -> None:
    """Open the device file with an editor."""
    file_path = device_manager.file_path
    cli_utils.edit_file(file_path, editor)
