""" Module that provided the ``preset`` CLI command for xrd-tools. """
import click
import logging
import os
import sys

sys.path.insert(0, os.path.abspath(".."))

from ..config import (
    DEVICES,
    PRESETS,
    EDITOR,
)
from ..preset_manager import PresetManager

from . import cli_utils
from .cli_meta import get_meta_kwargs

logger = logging.getLogger(__name__)
pass_preset_manager = click.make_pass_decorator(PresetManager)


@click.group()
@click.option(
    "--presets",
    type=click.Path(),
    default=PRESETS,
    required=True,
    show_default=True,
    help="Path to the preset file (JSON).",
)
@click.pass_context
def preset(ctx, presets: str):
    """CLI to manage measurement presets."""
    ctx.obj = PresetManager(file_path=presets)


@preset.command("add")
@click.option(
    "--name",
    type=click.STRING,
    required=True,
    prompt="Enter a name for the new preset",
    help="Measurement preset name.",
)
@click.option(
    "--devices",
    type=click.Path(),
    default=DEVICES,
    required=True,
    show_default=True,
    help="Path to the device file (JSON).",
)
@pass_preset_manager
def add_preset(pmanager: PresetManager, name: str, devices: str):
    "Create and register a new preset." ""
    new_preset = get_meta_kwargs(device_file=devices, create_preset=True)
    pmanager.add_preset(name, new_preset, to_file=True)


@preset.command("remove")
@click.option(
    "--name",
    type=click.STRING,
    help="Name of preset to be removed.",
)
@click.option(
    "--force",
    is_flag=True,
    default=False,
    show_default=True,
    help="Omit confirmation prompt.",
)
@pass_preset_manager
def remove_preset(pmanager: PresetManager, name: str, force: bool = False):
    "Remove a registered preset." ""
    choices = pmanager.get_names()
    if len(choices) == 0:
        logger.info("No preset registered.")
        return None
    if len(choices) == 1:
        name = choices[0]
    if name is None:
        msg = "Select preset to be removed:"
        name = cli_utils.select_option(choices, msg)
    if not force:
        click.confirm(
            f"Are you sure you want to remove '{name}'",
            abort=True,
            prompt_suffix="? ",
        )
    pmanager.remove_preset(name)


@preset.command("list")
@pass_preset_manager
def list_presets(pmanager: PresetManager):
    "List all registered presets." ""
    if not pmanager.has_preset:
        logger.info("No preset registered.")
        return None
    click.echo("Registered presets:")
    for preset in pmanager.get_names():
        click.echo(f" * {preset}")


@preset.command("edit")
@click.option(
    "--editor",
    type=click.STRING,
    default=EDITOR,
    show_default=True,
    help="Editor to be used.",
)
@pass_preset_manager
def edit_presets(pmanager: PresetManager, editor: str) -> None:
    """Open the preset file with an editor."""
    file_path = pmanager.file_path
    cli_utils.edit_file(file_path, editor)
