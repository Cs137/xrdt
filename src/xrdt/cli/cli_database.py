""" Module that provided the ``database`` CLI command for xrd-tools. """
import logging
import os
import sys

import click
import pandas as pd
from simple_term_menu import TerminalMenu

from . import cli_utils
from .cli_meta import create_meta_obj, create_protocol_cli, get_meta_kwargs

sys.path.insert(0, os.path.abspath(".."))
from ..analyse_measurements import AnalyseMeasurements
from ..config import (
    DATABASE,
    DEVICES,
    PRESETS,
    TEMPLATE_DIR,
    DEVICE_TYPES,
    EDITOR,
    ENCODING,
    MEASUREMENTS_DIR,
)
from ..database import MeasurementDatabase
from ..measurement_manager import MeasurementManager


logger = logging.getLogger(__name__)
pass_measurement_manager = click.make_pass_decorator(MeasurementManager)
pass_measurement_db = click.make_pass_decorator(MeasurementDatabase)


@click.group()
@click.option(
    "--db-file",
    type=click.Path(),
    default=DATABASE,
    show_default=True,
    help="Path to the database file (CSV).",
)
@pass_measurement_manager
@click.pass_context
def database(ctx, measurement_manager: MeasurementManager, db_file: str):
    """CLI to manage XRD measurements."""
    ctx.obj = MeasurementDatabase(db_file, MeasurementManager.measurements_dir)


@database.command("compare")
@pass_measurement_db
def compare_measurements(measurement_database: MeasurementDatabase):
    """Compare measurements registered in the DB."""
    options = list(measurement_database.entries.index)
    terminal_menu = TerminalMenu(
        options,
        title="Select measurements to compare:",
        multi_select=True,
        show_multi_select_hint=True,
    )
    menu_entry_indices = terminal_menu.show()
    ids = [options[i] for i in menu_entry_indices]
    measurements = [measurement_database._m_manager.get_measurement(i) for i in ids]
    am = AnalyseMeasurements(measurements)
    am.multiplot()


@database.command("compound")
@click.option(
    "--compound",
    type=click.STRING,
    default=None,
    help="Compound.",
)
@pass_measurement_db
def compound(measurement_database: MeasurementDatabase, compound: str):
    """List measurements of certain compound registered in DB."""
    if compound is None:
        compounds = measurement_database.list_compounds()
        compound = cli_utils.select_option(
            compounds, f"Select compound ({len(compounds)} registered)"
        )
    df = measurement_database.entries[
        measurement_database.entries["compound"] == compound
    ].sort_values("sample")
    click.echo(df)
    if len(df) == 0:
        raise ValueError(f"No measurements registered for '{compound}'")
    if not click.confirm("Plot measurement(s)?", default=True):
        sys.exit()
    if len(df) == 1:
        compare = False
        measurement_id = df.index[0]
    else:
        compare = click.confirm("Compare measurements?", default=True)
        if not compare:
            measurement_id = cli_utils.select_option(
                list(df.index), "Select measurement to plot"
            )
    if compare:
        am = AnalyseMeasurements.from_ids(df.index)
        am.multiplot()
    else:
        measurement = measurement_database._m_manager.get_measurement(measurement_id)
        measurement.plot()


@database.command("sample")
@click.option(
    "--name",
    type=click.STRING,
    default=None,
    help="ID of the sample.",
)
@pass_measurement_db
def sample(measurement_database: MeasurementDatabase, name: str):
    """List all measurements registered for a certain sample in the DB."""
    if name is None:
        samples = measurement_database.list_samples()
        name = cli_utils.select_option(
            samples, f"Select sample ID ({len(samples)} registered)"
        )
    df = measurement_database.entries[
        measurement_database.entries["sample"] == name
    ].sort_values("sample")
    click.echo(df)
    if len(df) == 0:
        raise ValueError(f"No measurements registered for '{name}'")
    if not click.confirm("Plot measurement(s)?", default=True):
        sys.exit()
    if len(df) == 1:
        compare = False
        measurement_id = df.index[0]
    else:
        compare = click.confirm("Compare measurements?", default=True)
        if not compare:
            measurement_id = cli_utils.select_option(
                list(df.index), "Select measurement to plot"
            )

    if compare:
        measurements = [
            measurement_database._m_manager.get_measurement(i) for i in df.index
        ]
        am = AnalyseMeasurements(measurements)
        am.multiplot()
    else:
        measurement = measurement_database._m_manager.get_measurement(measurement_id)
        measurement.plot()


@database.command("list")
@pass_measurement_db
def list_db_entries(measurement_database: MeasurementDatabase):
    """List DB file content."""
    df = measurement_database.entries
    if len(df) == 0:
        logger.info("No measurement registered.")
    else:
        for col in ["xrd_datetime", "date_added"]:
            df[col] = pd.to_datetime(df[col])
        click.echo(df)
