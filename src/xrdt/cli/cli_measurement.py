""" Module that provided the ``measurement`` CLI command for xrd-tools. """
import logging
import os
import sys

import click

from . import cli_utils
from .cli_meta import create_meta_obj, create_protocol_cli, get_meta_kwargs

sys.path.insert(0, os.path.abspath(".."))
from ..database import MeasurementDatabase
from ..preset_manager import PresetManager
from ..measurement_manager import MeasurementManager
from ..config import (
    AUTHOR,
    DATABASE,
    DEVICES,
    PRESETS,
    TEMPLATE_DIR,
    DEVICE_TYPES,
    EDITOR,
    ENCODING,
    MEASUREMENTS_DIR,
)


logger = logging.getLogger(__name__)
pass_measurement_manager = click.make_pass_decorator(MeasurementManager)
pass_measurement_db = click.make_pass_decorator(MeasurementDatabase)


@click.group()
def measurement():
    """CLI to manage a XRD measurement."""


@measurement.command("add")
@click.option(
    "--measurement-id",
    type=str,
    required=True,
    prompt="Enter an ID for the measurement",
    help="ID of the XRD measurement to add.",
)
@click.option(
    "--preset",
    type=str,
    default=None,
    help="Preset to be assigned to the XRD measurement.",
)
@click.option(
    "--source-file",
    type=click.Path(),
    default=None,
    help="Path to the measurement source data file (ASCII).",
)
@click.option(
    "--preset-file",
    type=click.Path(),
    default=PRESETS,
    required=True,
    show_default=True,
    help="Path to the preset file (JSON).",
)
@click.option(
    "--device-file",
    type=click.Path(),
    default=DEVICES,
    required=True,
    show_default=True,
    help="Path to the device file (JSON).",
)
@click.option(
    "--ht-mode/--no-ht-mode",
    default=False,
    show_default=True,
    help="flag indicating if a high-tmeperature or ambient measurement is added, is redefined by preset if assigned.",
)
@click.option(
    "--to-file/--no-file",
    default=True,
    show_default=True,
    help="flag indicating whether to write the XRD measurement to file(s).",
)
@click.option(
    "--plot/--no-plot",
    default=True,
    show_default=True,
    help="flag to plot XRD data after addition.",
)
@click.option(
    "--protocol",
    is_flag=True,
    default=False,
    help="flag to create measurement protocol.",
)
@click.option(
    "--zip-src/--no-zip-src",
    default=True,
    show_default=True,
    help="flag to indicate if archive with source data is added.",
)
@click.option(
    "--rm-src/--no-rm-src",
    default=False,
    show_default=True,
    help="flag to indicate if source data file is deleted.",
)
@click.option(
    "--force",
    is_flag=True,
    default=False,
    show_default=True,
    help="flag to allow overwriting of existing files and skip confirmation prompts.",
)
@click.option(
    "--author",
    type=str,
    default=AUTHOR,
    show_default=True,
    help="Name of the measurement protocol author, if it is created.",
)
@click.option(
    "--template-name",
    type=click.STRING,
    help="Name of registered protocol template to be used if it is created.",
)
@click.option(
    "--template_dir",
    type=click.Path(),
    default=TEMPLATE_DIR,
    show_default=True,
    help="Path to the template directory.",
)
@pass_measurement_manager
def add_measurement(
    mmanager: MeasurementManager,
    measurement_id: str,
    author: str,
    template_name: str,
    template_dir: str,
    preset: str,
    preset_file: str,
    protocol: bool,
    device_file: str,
    source_file: str,
    to_file: bool,
    plot: bool,
    zip_src: bool,
    rm_src: bool,
    ht_mode: bool,
    force: bool,
) -> None:
    """Add a XRD measurement."""
    meta_dict = {"measurement_id": measurement_id, "ht_mode": ht_mode}
    # Assign preset values if wanted, preset file exists and not creating a preset
    pmanager = PresetManager(file_path=preset_file)
    choices = pmanager.get_names()
    if os.path.isfile(pmanager.file_path):
        if len(choices) == 0:
            logger.info("No preset registered.")
        elif preset is None:
            if click.confirm(
                "Assign a preset to the measurement", default=True, prompt_suffix="? "
            ):
                if len(choices) == 1:
                    preset = choices[0]
                else:
                    preset = cli_utils.select_option(
                        pmanager.get_names(), title="Select preset:"
                    )
    if preset is not None:
        preset_values = pmanager.get_preset(preset)
        meta_dict.update(preset_values)
        logger.info(f"Assigned preset {preset!r} to measurement {measurement_id!r}.")

    # Create meta_obj
    meta_obj = create_meta_obj(
        meta_dict=meta_dict,
        device_file=device_file,
        to_file=to_file,
        force=force,
    )
    # Import data
    if source_file is None:
        if click.confirm(
            "Add XRD data from an ASCII file",
            default=True,
            prompt_suffix="? ",
        ):
            source_file = click.prompt(
                f"Enter path to data file",
                type=click.Path(exists=True),
            ).strip()

    measurement = mmanager.import_measurement(
        meta_obj=meta_obj,
        source_file=source_file,
        source_type="ascii",
        to_file=to_file,
        zip_source=zip_src,
        remove_source=rm_src,
        force=force,
        encoding=mmanager.encoding,
    )

    # Generate protocol if desired
    if protocol:
        create_protocol_cli(measurement, author, template_name, template_dir)

    # Plot the scan if desired
    if plot and source_file is not None:
        measurement.plot()


@measurement.command("plot")
@click.option(
    "--measurement-id", type=click.STRING, help="ID of the XRD measurement to plot."
)
@click.option(
    "--norm", is_flag=True, default=False, help="Plot the normalised XRD data."
)
@pass_measurement_manager
def plot(
    measurement_manager: MeasurementManager, measurement_id: str, norm: bool
) -> None:
    """Plot a XRD measurement."""
    if measurement_id is None:
        msg = "Select a measurement to plot:"
        measurement_id = cli_utils.select_option(
            cli_utils.list_measurements(MEASUREMENTS_DIR),
            msg,
            show_search_hint_text=True,
        )
    measurement = measurement_manager.get_measurement(measurement_id)
    measurement.plot(norm=norm)


@measurement.command("refine")
@click.option(
    "--measurement-id",
    type=str,
    prompt=False,
    required=False,
    help="ID of the XRD measurement to refine.",
)
@click.option(
    "--refiner",
    type=str,
    default="profex",
    show_default=True,
    help="Refinement interface plugin to be used.",
)
@pass_measurement_manager
def refine(
    measurement_manager: MeasurementManager, measurement_id: str, refiner: str
) -> None:
    """Refine a XRD measurement with Profex.

    This command requires a working installation of Profex on your computer.
    """
    # Select measurement if not provided
    if measurement_id is None:
        msg = "Select a measurement to refine:"
        measurement_id = cli_utils.select_option(
            cli_utils.list_measurements(MEASUREMENTS_DIR),
            msg,
            show_search_hint_text=True,
        )

    # get measurement instance
    measurement = measurement_manager.get_measurement(measurement_id)
    measurement.set_refinement_interface(refiner)
    measurement.refine()


@measurement.command("protocol")
@click.option(
    "--measurement-id",
    type=str,
    prompt=False,
    required=False,
    help="ID of the XRD measurement.",
)
@click.option(
    "--author",
    type=str,
    default=AUTHOR,
    show_default=True,
    help="Name of the author.",
)
@click.option(
    "--editor",
    type=str,
    default=EDITOR,
    show_default=True,
    help="Editor to be used.",
)
@click.option(
    "--template-name",
    type=click.Path(exists=True),
    help="Name of registered measurement protocol template.",
)
@click.option(
    "--template_dir",
    type=click.Path(),
    default=TEMPLATE_DIR,
    show_default=True,
    help="Path to the template directory.",
)
@pass_measurement_manager
def create_measurement_protocol(
    mmanager: MeasurementManager,
    measurement_id: str,
    author: str,
    editor: str,
    template_name: str,
    template_dir: str,
) -> None:
    """(Create and) open a XRD measurement protocol."""

    # Get the Measurement object, select measurement_id if not provided
    if measurement_id is None:
        measurement_id = cli_utils.select_option(
            cli_utils.list_measurements(MEASUREMENTS_DIR),
            "Choose a measurement to open (and create) a protocol for:",
            show_search_hint_text=True,
        )
    measurement = mmanager.get_measurement(measurement_id)

    # Create protocol if it does not exist
    if not os.path.isfile(measurement.paths.file_protocol):
        create_protocol_cli(measurement, author, template_name, template_dir)

    # Open protocol with editor
    cli_utils.edit_file(measurement.paths.file_protocol, editor)
