""" Module that provided the ``template`` CLI command for xrd-tools. """
import inspect
import logging
import os
import sys

import click

from . import cli_utils

sys.path.insert(0, os.path.abspath(".."))
from ..templates import DEFAULT_TEMPLATES, TemplateManager
from ..config import (
    TEMPLATE_DIR,
    EDITOR,
)

logger = logging.getLogger(__name__)
pass_template_manager = click.make_pass_decorator(TemplateManager)


@click.group()
@click.option(
    "--template_dir",
    type=click.Path(),
    default=TEMPLATE_DIR,
    show_default=True,
    help="Path to the template directory.",
)
@click.pass_context
def template(ctx, template_dir: str):
    """CLI to manage measurement document templates."""
    ctx.obj = TemplateManager(template_dir)


@template.command("add")
@click.option(
    "--name",
    type=click.STRING,
    required=True,
    prompt="Enter a name for the new template",
    help="Name of the template to be created.",
)
@click.option(
    "--document-type",
    type=click.Choice(DEFAULT_TEMPLATES.keys()),
    default=None,
    help="Document type for the template to be created.",
)
@click.option(
    "--editor",
    type=str,
    default=EDITOR,
    show_default=True,
    help="Editor to be used.",
)
@click.option(
    "--force",
    is_flag=True,
    default=False,
    show_default=True,
    help="flag to allow overwriting of existing template file.",
)
@pass_template_manager
def create_template(
    template_manager: TemplateManager,
    name: str,
    document_type: str,
    editor: str,
    force: bool,
):
    """Create and register a new document template."""
    if document_type is None:
        if len(template_manager.types) == 0:
            logger.debug("No document type registered.")
            return None
        if len(template_manager.types) == 1:
            document_type = template_manager.types[0]
        else:
            msg = "Choose a document type for the new template:"
            document_type = cli_utils.select_option(template_manager.types, msg)
    template_manager.create_template(name, document_type, force)
    file_path = template_manager.get_template(document_type, name)
    cli_utils.edit_file(file_path, editor)


@template.command("list")
@click.option(
    "--document-type",
    type=click.Choice(DEFAULT_TEMPLATES.keys()),
    default=None,
    help="Document type for the template to be created.",
)
@pass_template_manager
def list_templates(template_manager: TemplateManager, document_type: str):
    """List registered templates, optionally by type."""
    if document_type is None:
        templates = template_manager.get_type_name_list()
    else:
        templates = template_manager.get_templates(document_type, names=True)
    if len(templates) == 0:
        logger.info("No measurement document template registered.")
        return None
    click.echo("Registered templates:")
    for template in templates:
        click.echo(f" * {template}")


@template.command("remove")
@click.option(
    "--name",
    type=str,
    help="Name of the template to be removed.",
)
@click.option(
    "--force",
    is_flag=True,
    default=False,
    help="Skip the confirmation prompt.",
)
@pass_template_manager
def remove_template(
    template_manager: TemplateManager, name: str = None, force: bool = False
) -> None:
    """Remove a registered template."""
    if name is None:
        choices = template_manager.get_type_name_list()
        if len(choices) == 0:
            logger.debug("No template registered.")
            return None
        elif len(choices) == 1:
            document_type, name = choices[0].split(template_manager.separator)
        else:
            msg = "Select a template to be removed:"
            selection = cli_utils.select_option(choices, msg)
            document_type, name = selection.split(template_manager.separator)
    else:
        document_type = template_manager.get_type(name)
    if not force:
        click.confirm(
            f"Are you sure you want to remove {name!r} ({document_type})",
            abort=True,
            prompt_suffix="? ",
        )
    template_manager.remove_template(document_type, name)


@template.command("edit")
@click.option(
    "--name",
    type=str,
    help="Name of the template to be edited.",
)
@click.option(
    "--editor",
    type=str,
    default=EDITOR,
    show_default=True,
    help="Editor to be used.",
)
@pass_template_manager
def edit_templates(template_manager: TemplateManager, name: str, editor: str):
    """Edit a registered measurement document template."""
    if name is None:
        choices = template_manager.get_type_name_list()
        if len(choices) == 0:
            logger.debug("No measurement document template registered.")
            return None
        elif len(choices) == 1:
            document_type, name = choices[0].split(template_manager.separator)
        else:
            msg = "Select a template to be edited:"
            selection = cli_utils.select_option(choices, msg)
            document_type, name = selection.split(template_manager.separator)
    if document_type is None:
        document_type = template_manager.get_type(name)
    file_path = template_manager.get_template(document_type, name)
    cli_utils.edit_file(file_path, editor)
