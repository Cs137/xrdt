"""Utility functions for xrd-tools CLI."""
import logging
import os
import subprocess
import sys

import click
from simple_term_menu import TerminalMenu

sys.path.insert(0, os.path.abspath(".."))
from ..utils import make_dirs

logger = logging.getLogger(__name__)


def confirm_file_replacement(file_path) -> None:
    """Confirm to replace existing file, abort if not."""
    if os.path.isfile(file_path):
        click.confirm(
            f"File '{os.path.basename(file_path)}' already exists! Replace it",
            default=False,
            abort=True,
            prompt_suffix="? ",
        )


def edit_file(file_path: str, editor: str) -> None:
    """
    Open a file with the specified editor.

    :param file_path: Path to the protocol file.
    :param editor:    Editor to open protocol file.
    """
    logger.debug(f"Opening {file_path!r} with {editor!r}...")
    subprocess.run([editor, file_path])


def list_measurements(measurements_dir) -> list[str]:
    """Returns list of measurements in measurements_dir.

    Sorted reversly, newest is first item in list.
    """
    return sorted(
        [
            os.path.basename(item.path)
            for item in os.scandir(measurements_dir)
            if item.is_dir()
        ],
        reverse=True,
    )


def select_option(
    options: list, title: str = None, return_index=False, **kwargs
) -> int | str:
    """CLI to get a selection from a list of options.

    :options:      List of selection options
    :title:        Title of selection
    :return_index: Return index instead of list item
    :kwargs:       Forwarded to TerminalMenu instance initiation
    """
    terminal_menu = TerminalMenu(options, title=title, **kwargs)
    i = terminal_menu.show()
    if i is None:
        logger.debug("Selection aborted, no valid choice.")
        raise click.Abort()
    if return_index:
        return i
    return options[i]
