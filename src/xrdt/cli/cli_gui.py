""" Module that provided the ``gui`` CLI command for xrd-tools. """

import logging
import os
import sys

import click
from PyQt5.QtWidgets import QApplication

from xrdt.gui.__main__ import MainWindow
from xrdt.measurement_manager import MeasurementManager

logger = logging.getLogger(__name__)
pass_measurement_manager = click.make_pass_decorator(MeasurementManager)


@click.command("gui")
@click.option("--measurement-id", type=str, help="ID of the XRD measurement to open.")
@pass_measurement_manager
def gui(m_manager: MeasurementManager, measurement_id: str):
    """XRD tools GUI."""
    logger.debug(f"Loading user interface...")
    app = QApplication(sys.argv)
    window = MainWindow(m_manager, measurement_id)
    window.show()

    sys.exit(app.exec_())
    xrdt_gui(m_manager, measurement_id)
