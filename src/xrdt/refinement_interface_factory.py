"""Factory to create a refinement interface glugin."""
import logging
from typing import Any, Callable, Protocol

from .refinement_interface import RefinementInterface

logger = logging.getLogger(__name__)

creation_funcs: dict[str, Callable[..., RefinementInterface]] = {}


def register(name: str, creator_fn: Callable[..., RefinementInterface]) -> None:
    """Register a new refinement interface."""
    creation_funcs[name] = creator_fn
    logger.debug(f"Registered refinement interface {name!r}.")


def unregister(name: str) -> None:
    """Unregister a plugin type."""
    creation_funcs.pop(name, None)
    logger.debug(f"Unregistered refinement interface {name!r}.")


def create(arguments: dict[str, Any]) -> RefinementInterface:
    """Create a plugin of a specific type, given JSON data."""
    args_copy = arguments.copy()
    name = args_copy.pop("name")
    try:
        creator_funcs = creation_funcs[name]
    except KeyError:
        raise ValueError(f"Unknown refinement interface {name!r}") from None
    logger.debug(f"Creating refinement interface {name!r}.")
    return creator_funcs(**args_copy)
